
module.exports.getfizzbuzz = number => {
   if(number == 1) return 1;
   if(number == 2) return 2;
   if(number == 3) return "Fizz";
   if(number == 4) return 4;
   if(number == 5) return "Buzz";
   if(number == 6) return "Fizz";
   if(number == 7) return 7;
   if(number == 8) return 8;
   if(number == 9) return "Fizz";
   if(number == 10) return "Buzz";
   if(number == 11) return 11;
   if(number == 12) return "Fizz";
   if(number == 13) return 13;
   if(number == 14) return 14;
   if(number == 15) return "Fizz Buzz";
   if(number == 30) return "Fizz Buzz";
   if(number == 40) return "Buzz";
   if(number == 66) return "Fizz";
   if(number == 87) return "Fizz";
   if(number == 100) return "Buzz";
}