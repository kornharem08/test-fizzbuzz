const fizzbuzz = require('../../src/fizzbuzz');
describe('fizzbuzz', () => {
    it('should return 1 if number 1', () => {
        const number = 1;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(1);
    })
    it('should return 2 if number 2', () => {
        const number = 2;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(2);
    })
    it('should return Fizz if number 3', () => {
        const number = 3;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz");
    })
    it('should return 4 if number 4', () => {
        const number = 4;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(4);
    })
    it('should return Buzz if number 5', () => {
        const number = 5;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Buzz");
    })
    it('should return Fizz if number 6', () => {
        const number = 6;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz");
    })
    it('should return 7 if number 7', () => {
        const number = 7;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(7);
    })
    it('should return 8 if number 8', () => {
        const number = 8;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(8);
    })
    it('should return Fizz if number 9', () => {
        const number = 9;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz");
    })
    it('should return Buzz if number 10', () => {
        const number = 10;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Buzz");
    })
    it('should return 11 if number 11', () => {
        const number = 11;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(11);
    })
    it('should return Fizz if number 12', () => {
        const number = 12;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz");
    })
    it('should return 13 if number 13', () => {
        const number = 13;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(13);
    })
    it('should return 14 if number 14', () => {
        const number = 14;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe(14);
    })
    it('should return Fizz Buzz if number 15', () => {
        const number = 15;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz Buzz");
    })
    it('should return Fizz Buzz if number 30', () => {
        const number = 30;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz Buzz");
    })
    it('should return Buzz if number 40', () => {
        const number = 40;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Buzz");

    })
    it('should return Fizz if number 66', () => {
        const number = 66;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz");
    })
    it('should return Fizz if number 87', () => {
        const number = 87;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Fizz");
    })
    it('should return Fizz if number 100', () => {
        const number = 100;
        const result = fizzbuzz.getfizzbuzz(number);
        expect(result).toBe("Buzz");
    })
})